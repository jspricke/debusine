# Copyright 2024 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Tests for the workspace views."""

from typing import ClassVar

from rest_framework import status

from debusine.artifacts.models import CollectionCategory
from debusine.db.context import context
from debusine.db.models import Collection, Scope, Workspace
from debusine.db.playground import scenarios
from debusine.test.django import (
    AllowAll,
    DenyAll,
    TestCase,
    override_permission,
)
from debusine.web.views.tests.utils import ViewTestMixin


class WorkspaceViewsTests(ViewTestMixin, TestCase):
    """Tests for Workspace views."""

    scenario = scenarios.DefaultScopeUser()

    only_public_workspaces_message = (
        "Not authenticated. Only public workspaces are listed."
    )

    workspace_public: ClassVar[Workspace]
    workspace_private: ClassVar[Workspace]
    collection: ClassVar[Collection]

    @classmethod
    @context.disable_permission_checks()
    def setUpTestData(cls) -> None:
        """Set up a database layout for views."""
        super().setUpTestData()
        cls.workspace_public = cls.playground.create_workspace(
            name="Public", public=True
        )
        cls.workspace_private = cls.playground.create_workspace(name="Private")

        cls.collection = cls.playground.create_collection(
            "Public",
            CollectionCategory.WORKFLOW_INTERNAL,
            workspace=cls.workspace_public,
        )

    def test_detail(self) -> None:
        """Test workspace detail view."""
        response = self.client.get(self.workspace_public.get_absolute_url())
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertHTMLValid(response)

    def test_detail_permissions(self) -> None:
        """Test permissions on workspace list view."""
        self.assertSetsCurrentWorkspace(
            self.workspace_public,
            self.workspace_public.get_absolute_url(),
        )

    def test_detail_can_create_artifact(self) -> None:
        """User is logged in and can create artifacts."""
        self.client.force_login(self.scenario.user)
        with (
            override_permission(Workspace, "can_create_artifacts", AllowAll),
            override_permission(Workspace, "can_create_work_requests", DenyAll),
        ):
            response = self.client.get(self.workspace_public.get_absolute_url())
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        tree = self.assertHTMLValid(response)
        self.assertHasElement(tree, "//a[@id='nav-create-artifact']")
        self.assertFalse(tree.xpath("//a[@id='nav-create-work-request']"))

    def test_detail_can_create_work_request(self) -> None:
        """User is logged in and can create work_request."""
        self.client.force_login(self.scenario.user)
        with (
            override_permission(Workspace, "can_create_artifacts", DenyAll),
            override_permission(
                Workspace, "can_create_work_requests", AllowAll
            ),
        ):
            response = self.client.get(self.workspace_public.get_absolute_url())
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        tree = self.assertHTMLValid(response)
        self.assertFalse(tree.xpath("//a[@id='nav-create-artifact']"))
        self.assertHasElement(tree, "//a[@id='nav-create-work-request']")

    def test_detail_private(self) -> None:
        """Unauthenticated detail view cannot access private workspaces."""
        response = self.client.get(self.workspace_private.get_absolute_url())
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        self.client.force_login(self.playground.get_default_user())
        response = self.client.get(self.workspace_private.get_absolute_url())
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        self.playground.create_group_role(
            self.workspace_private,
            Workspace.Roles.OWNER,
            self.playground.get_default_user(),
        )
        response = self.client.get(self.workspace_private.get_absolute_url())
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_name_duplicate(self) -> None:
        """Homonymous workspace in different scope does not trigger a dup."""
        scope = Scope.objects.create(name="tests")
        with context.disable_permission_checks():
            self.playground.create_workspace(
                name="Public",
                public=True,
                scope=scope,
            )
        response = self.client.get(self.workspace_public.get_absolute_url())
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertHTMLValid(response)

    def test_detail_workflowtemplate_list_empty(self) -> None:
        """Test showing a workspace with no workflow templates."""
        response = self.client.get(self.workspace_public.get_absolute_url())
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        tree = self.assertHTMLValid(response)
        div = self.assertHasElement(tree, "//div[@id='workflow-templates']")
        self.assertTextContentEqual(
            div, "No workflow templates configured for this workspace."
        )

    def test_detail_workflowtemplate(self) -> None:
        """Test showing a workspace with one workflow templates."""
        self.playground.create_workflow_template(
            name="Build package",
            task_name="sbuild",
            task_data={"prefix": "test"},
            workspace=self.workspace_public,
        )
        response = self.client.get(self.workspace_public.get_absolute_url())
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        tree = self.assertHTMLValid(response)
        div = self.assertHasElement(tree, "//div[@id='workflow-templates']")
        self.assertTextContentEqual(
            div.div.div[0], "Build package (type sbuild, priority 0)"
        )
        self.assertTextContentEqual(div.div.div[1], "prefix: test")
