# Copyright 2024 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Tests for workflows."""

from unittest.mock import Mock

from debusine.db.models import WorkRequest
from debusine.test.django import TestCase
from debusine.web.templatetags.workflows import workflow_runtime_status_small


class WorkflowRuntimeStatusSmallTests(TestCase):
    """Tests for workflow_runtime_status_small tag."""

    def setUp(self) -> None:
        """Set up test."""
        super().setUp()
        self.workflow = Mock(spec=WorkRequest)

    def test_status_is_completed(self) -> None:
        """Test workflow status is completed."""
        self.workflow.status = WorkRequest.Statuses.COMPLETED
        self.workflow.workflow_runtime_status = (
            WorkRequest.RuntimeStatuses.COMPLETED
        )

        actual = workflow_runtime_status_small(self.workflow)

        self.assertHTMLEqual(
            actual,
            '<span class="badge text-bg-primary" title="Completed">C</span>',
        )

    def test_status_is_running(self) -> None:
        """Test workflow status is running and runtime status is blocked."""
        self.workflow.status = WorkRequest.Statuses.RUNNING
        self.workflow.workflow_runtime_status = (
            WorkRequest.RuntimeStatuses.BLOCKED
        )

        actual = workflow_runtime_status_small(self.workflow)

        self.assertHTMLEqual(
            actual,
            '<span class="badge text-bg-dark" title="Blocked">B</span>',
        )
