# Copyright 2024 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Template tag library for workflows."""
from django import template
from django.utils.html import format_html

from debusine.db.models import WorkRequest

register = template.Library()


@register.simple_tag
def workflow_runtime_status_small(workflow: WorkRequest) -> str:
    """
    Render workflow runtime status component.

    If the status is not running: returns badge with one-letter for the
    Status.
    If the status is running: returns badge with the RuntimeStatus.
    """
    statuses_formatting = {
        WorkRequest.Statuses.PENDING: ("Pending", "secondary", "P"),
        WorkRequest.Statuses.RUNNING: ("Running", "secondary", "R"),
        WorkRequest.Statuses.COMPLETED: ("Completed", "primary", "C"),
        WorkRequest.Statuses.ABORTED: ("Aborted", "dark", "A"),
        WorkRequest.Statuses.BLOCKED: ("Blocked", "dark", "B"),
    }

    runtime_status_formatting = {
        WorkRequest.RuntimeStatuses.NEEDS_INPUT: (
            "Input needed",
            "secondary",
            "I",
        ),
        WorkRequest.RuntimeStatuses.RUNNING: ("Running", "secondary", "R"),
        WorkRequest.RuntimeStatuses.WAITING: ("Waiting", "secondary", "W"),
        WorkRequest.RuntimeStatuses.PENDING: ("Pending", "secondary", "P"),
        WorkRequest.RuntimeStatuses.ABORTED: ("Aborted", "dark", "A"),
        WorkRequest.RuntimeStatuses.COMPLETED: ("Completed", "primary", "C"),
        WorkRequest.RuntimeStatuses.BLOCKED: ("Blocked", "dark", "B"),
    }

    if workflow.status == WorkRequest.Statuses.RUNNING:
        # Return badge with the workflow.workflow_runtime_status only
        assert workflow.workflow_runtime_status

        runtime_status_title, runtime_status_color, runtime_status_text = (
            runtime_status_formatting[
                WorkRequest.RuntimeStatuses(workflow.workflow_runtime_status)
            ]
        )

        return format_html(
            ' <span title="{runtime_status_title}" '
            'class="badge text-bg-{runtime_status_color}">'
            '{runtime_status_text}</span>',
            runtime_status_title=runtime_status_title,
            runtime_status_color=runtime_status_color,
            runtime_status_text=runtime_status_text,
        )

    status_title, status_color, status_text = statuses_formatting[
        WorkRequest.Statuses(workflow.status)
    ]

    html = format_html(
        '<span title="{status_title}" class="badge text-bg-{status_color}">'
        '{status_text}</span>',
        status_title=status_title,
        status_color=status_color,
        status_text=status_text,
    )

    return html
