# Generated by Django 4.2.15 on 2024-11-14 15:16

from django.db import migrations, models

import debusine.db.models.collections


class Migration(migrations.Migration):
    dependencies = [
        ('db', '0099_delete_dynamic_data_workflows'),
    ]

    operations = [
        migrations.AlterField(
            model_name='collection',
            name='name',
            field=models.CharField(
                max_length=255,
                validators=[
                    debusine.db.models.collections.validate_collection_name
                ],
            ),
        ),
        migrations.AlterField(
            model_name='notificationchannel',
            name='name',
            field=models.CharField(
                max_length=20,
                unique=True,
                validators=[
                    debusine.db.models.work_requests.validate_notification_channel_name
                ],
            ),
        ),
    ]
