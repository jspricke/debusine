# Generated by Django 4.2.15 on 2024-10-18 10:30

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('db', '0089_group_name_validation'),
    ]

    operations = [
        migrations.CreateModel(
            name='WorkspaceRole',
            fields=[
                (
                    'id',
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name='ID',
                    ),
                ),
                (
                    'role',
                    models.CharField(
                        choices=[('owner', 'Owner')], max_length=16
                    ),
                ),
                (
                    'group',
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.PROTECT,
                        related_name='workspace_roles',
                        to='db.group',
                    ),
                ),
                (
                    'resource',
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.PROTECT,
                        related_name='roles',
                        to='db.workspace',
                    ),
                ),
            ],
        ),
        migrations.AddConstraint(
            model_name='workspacerole',
            constraint=models.UniqueConstraint(
                fields=('resource', 'group', 'role'),
                name='db_workspacerole_unique_resource_group_role',
            ),
        ),
    ]
