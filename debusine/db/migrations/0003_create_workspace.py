# Generated by Django 3.2.12 on 2022-09-05 15:00

from django.db import migrations
from django.db.backends.base.schema import BaseDatabaseSchemaEditor
from django.db.migrations.state import StateApps

from debusine.db.models import DEFAULT_WORKSPACE_NAME, default_file_store


def create_system_workspace(
    apps: StateApps, schema_editor: BaseDatabaseSchemaEditor
) -> None:
    Workspace = apps.get_model("db", "Workspace")

    # It uses default_file_store_id because during the migrations
    # Workspace.default_file_store is of type "__fake__.Workspace" instead of
    # Workspace and it fails a check when creating the object.
    # This is partially explained in https://code.djangoproject.com/ticket/24282
    Workspace.objects.create(
        name=DEFAULT_WORKSPACE_NAME,
        default_file_store_id=default_file_store().id,
    )


class Migration(migrations.Migration):
    dependencies = [
        ('db', '0002_create_default_store'),
    ]

    operations = [migrations.RunPython(create_system_workspace)]
