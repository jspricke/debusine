# Copyright 2024 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Unit tests for the debian pipeline workflow."""
from typing import Any, ClassVar
from unittest.mock import call, patch

from debusine.artifacts.models import (
    ArtifactCategory,
    CollectionCategory,
    KeyPurpose,
)
from debusine.client.models import LookupChildType
from debusine.db.context import context
from debusine.db.models import Artifact, TaskDatabase, WorkRequest
from debusine.server.workflows import DebianPipelineWorkflow
from debusine.server.workflows.base import (
    WorkflowRunError,
    orchestrate_workflow,
)
from debusine.server.workflows.models import DebianPipelineWorkflowData
from debusine.tasks.models import (
    BackendType,
    BaseDynamicTaskData,
    CollectionItemMatcherKind,
    LookupMultiple,
    TaskTypes,
)
from debusine.test.django import TestCase


class DebianPipelineWorkflowTests(TestCase):
    """Unit tests for :py:class:`DebianPipelineWorkflow`."""

    source_artifact: ClassVar[Artifact]

    @classmethod
    @context.disable_permission_checks()
    def setUpTestData(cls) -> None:
        """Set up common data."""
        super().setUpTestData()
        cls.source_artifact = cls.playground.create_source_artifact(
            name="hello"
        )

    def create_debian_pipeline_workflow(
        self, *, extra_task_data: dict[str, Any]
    ) -> DebianPipelineWorkflow:
        """Create a debian pipeline workflow."""
        task_data = {
            "source_artifact": 10,
            "vendor": "debian",
            "codename": "bookworm",
        }
        task_data.update(extra_task_data)
        wr = self.playground.create_workflow(
            task_name="debian_pipeline", task_data=task_data
        )
        return DebianPipelineWorkflow(wr)

    def orchestrate(self, extra_data: dict[str, Any]) -> WorkRequest:
        """Create and orchestrate a DebianPipelineWorkflow."""
        data = {
            "source_artifact": 10,
            "vendor": "debian",
            "codename": "bookworm",
            "enable_autopkgtest": False,
            "enable_lintian": False,
            "enable_piuparts": False,
            **extra_data,
        }

        wr = self.playground.create_workflow(
            task_name="debian_pipeline",
            task_data=DebianPipelineWorkflowData(**data),
        )
        self.assertEqual(wr.status, WorkRequest.Statuses.PENDING)
        wr.mark_running()
        orchestrate_workflow(wr)

        return wr

    def test_compute_dynamic_data(self) -> None:
        """Dynamic data includes a parameter summary."""
        source_artifact = self.playground.create_source_artifact(
            name="hello", version="1.0-1"
        )
        wr = self.playground.create_workflow(
            task_name="debian_pipeline",
            task_data=DebianPipelineWorkflowData(
                source_artifact=source_artifact.id,
                vendor="debian",
                codename="sid",
            ),
        )
        workflow = DebianPipelineWorkflow(wr)

        self.assertEqual(
            workflow.compute_dynamic_data(TaskDatabase(wr)),
            BaseDynamicTaskData(parameter_summary="hello_1.0-1"),
        )

    def test_compute_dynamic_data_source_artifact_wrong_category(self) -> None:
        """`source_artifact` must be a `debian:source-package`."""
        artifact, _ = self.create_artifact(category=ArtifactCategory.TEST)
        wr = self.playground.create_workflow(
            task_name="debian_pipeline",
            task_data=DebianPipelineWorkflowData(
                source_artifact=artifact.id, vendor="debian", codename="sid"
            ),
        )
        workflow = DebianPipelineWorkflow(wr)

        with self.assertRaisesRegex(
            ValueError,
            f"source artifact is type '{ArtifactCategory.TEST}' instead of "
            f"{ArtifactCategory.SOURCE_PACKAGE}",
        ):
            workflow.compute_dynamic_data(TaskDatabase(wr))

    def test_populate_use_available_architectures(self) -> None:
        """
        Test populate use available architectures.

        The user didn't specify "architectures", DebianPipelineWorkflow
        checks available architectures and "all" and use them.
        """
        with context.disable_permission_checks():
            collection = self.playground.create_collection(
                "debian",
                CollectionCategory.ENVIRONMENTS,
                workspace=self.playground.get_default_workspace(),
            )
            for arch in ["amd64", "i386"]:
                artifact, _ = self.create_artifact(
                    category=ArtifactCategory.SYSTEM_TARBALL,
                    data={"codename": "bookworm", "architecture": arch},
                )
                collection.manager.add_artifact(
                    artifact, user=self.playground.get_default_user()
                )

        workflow = self.orchestrate(
            extra_data={
                "source_artifact": self.source_artifact.id,
                "vendor": "debian",
                "codename": "bookworm",
            }
        )

        sbuild = workflow.children.get(
            task_name="sbuild", task_type=TaskTypes.WORKFLOW
        )

        self.assertEqual(
            sbuild.task_data["architectures"], ["all", "amd64", "i386"]
        )

    def test_populate_sbuild(self) -> None:
        """Test populate create sbuild."""
        workflow = self.orchestrate(
            extra_data={
                "architectures": ["amd64"],
                "source_artifact": self.source_artifact.id,
            }
        )

        sbuild = workflow.children.get(
            task_name="sbuild", task_type=TaskTypes.WORKFLOW
        )

        self.assertEqual(sbuild.status, WorkRequest.Statuses.RUNNING)
        self.assertEqual(
            sbuild.task_data,
            {
                "arch_all_host_architecture": "amd64",
                "architectures": ["amd64"],
                "backend": BackendType.AUTO,
                "environment_variant": None,
                "extra_repositories": None,
                "input": {"source_artifact": self.source_artifact.id},
                "target_distribution": "debian:bookworm",
                "signing_template_names": {},
            },
        )
        self.assertEqual(
            sbuild.workflow_data_json,
            {"display_name": "sbuild", "step": "sbuild"},
        )

        # SbuildWorkflow.populate() was called and created its tasks
        self.assertTrue(sbuild.children.exists())
        for child in sbuild.children.all():
            self.assertEqual(child.status, WorkRequest.Statuses.PENDING)

        self.assertFalse(
            workflow.children.filter(
                task_name="qa", task_type=TaskTypes.WORKFLOW
            ).exists()
        )

    def assert_qa(
        self, workflow: WorkRequest, task_data: dict[str, Any]
    ) -> None:
        """Assert workflow has a sub-workflow qa with task_data."""
        qa = workflow.children.get(task_name="qa", task_type=TaskTypes.WORKFLOW)
        self.assertEqual(qa.status, WorkRequest.Statuses.RUNNING)
        self.assertEqual(qa.task_data, task_data)

        self.assertEqual(
            qa.workflow_data_json, {"display_name": "QA", "step": "qa"}
        )

        self.assertQuerySetEqual(
            qa.dependencies.all(),
            list(
                WorkRequest.objects.filter(
                    task_type=TaskTypes.WORKER, task_name="sbuild"
                )
            ),
        )

        # QaWorkflow.populate() was called and created its tasks
        self.assertTrue(qa.children.exists())
        for child in qa.children.all():
            # Children are sub-workflows, so go straight to RUNNING.
            self.assertEqual(child.status, WorkRequest.Statuses.RUNNING)

    def test_populate_qa_with_sbuild_subset(self) -> None:
        """`sbuild` may only create a subset of the requested architectures."""
        self.assertEqual(
            self.source_artifact.data["dsc_fields"]["Architecture"], "any"
        )

        workflow = self.orchestrate(
            extra_data={
                "architectures": ["all", "amd64"],
                "source_artifact": f"{self.source_artifact.id}@artifacts",
                "enable_lintian": True,
                "extra_repositories": [
                    {
                        "url": "http://example.com/",
                        "suite": "bookworm",
                        "components": ["main"],
                    }
                ],
            }
        )

        sbuild = workflow.children.get(
            task_name="sbuild", task_type=TaskTypes.WORKFLOW
        )

        self.assertEqual(
            sbuild.task_data,
            {
                "arch_all_host_architecture": "amd64",
                "architectures": ["all", "amd64"],
                "backend": BackendType.AUTO,
                "environment_variant": None,
                "input": {
                    "source_artifact": f"{self.source_artifact.id}@artifacts"
                },
                "target_distribution": "debian:bookworm",
                "signing_template_names": {},
                "extra_repositories": [
                    {
                        "url": "http://example.com/",
                        "suite": "bookworm",
                        "components": ["main"],
                    }
                ],
            },
        )
        self.assertEqual(
            sbuild.workflow_data_json,
            {"display_name": "sbuild", "step": "sbuild"},
        )

        self.assertTrue(sbuild.children.exists())
        self.assert_qa(
            workflow,
            {
                "arch_all_host_architecture": "amd64",
                "architectures": ["all", "amd64"],
                "autopkgtest_backend": BackendType.AUTO,
                "binary_artifacts": [
                    {
                        "collection": "internal@collections",
                        "child_type": LookupChildType.ARTIFACT_OR_PROMISE,
                        "name_matcher": {
                            "kind": CollectionItemMatcherKind.STARTSWITH,
                            "value": "build-",
                        },
                    }
                ],
                "codename": "bookworm",
                "extra_repositories": [
                    {
                        "url": "http://example.com/",
                        "suite": "bookworm",
                        "components": ["main"],
                    }
                ],
                "enable_autopkgtest": False,
                "enable_lintian": True,
                "enable_piuparts": False,
                "enable_reverse_dependencies_autopkgtest": False,
                "lintian_backend": BackendType.AUTO,
                "lintian_fail_on_severity": "none",
                "piuparts_backend": BackendType.AUTO,
                "reverse_dependencies_autopkgtest_suite": None,
                "source_artifact": f"{self.source_artifact.id}@artifacts",
                "vendor": "debian",
            },
        )

    def test_populate_qa_lintian(self) -> None:
        """Test populate create qa: lintian enabled."""
        workflow = self.orchestrate(
            extra_data={
                "architectures": ["amd64"],
                "source_artifact": self.source_artifact.id,
                "enable_lintian": True,
            }
        )
        self.assert_qa(
            workflow,
            {
                "arch_all_host_architecture": "amd64",
                "architectures": ["amd64"],
                "autopkgtest_backend": BackendType.AUTO,
                "binary_artifacts": [
                    {
                        "collection": "internal@collections",
                        "child_type": LookupChildType.ARTIFACT_OR_PROMISE,
                        "name_matcher": {
                            "kind": CollectionItemMatcherKind.STARTSWITH,
                            "value": "build-",
                        },
                    }
                ],
                "codename": "bookworm",
                "extra_repositories": None,
                "enable_autopkgtest": False,
                "enable_lintian": True,
                "enable_piuparts": False,
                "enable_reverse_dependencies_autopkgtest": False,
                "lintian_backend": BackendType.AUTO,
                "lintian_fail_on_severity": "none",
                "piuparts_backend": BackendType.AUTO,
                "reverse_dependencies_autopkgtest_suite": None,
                "source_artifact": self.source_artifact.id,
                "vendor": "debian",
            },
        )

    def test_populate_qa_autopkgtest(self) -> None:
        """Test populate create qa: autopkgtest enabled."""
        workflow = self.orchestrate(
            extra_data={
                "architectures": ["amd64"],
                "source_artifact": self.source_artifact.id,
                "enable_autopkgtest": True,
            }
        )

        self.assert_qa(
            workflow,
            {
                "arch_all_host_architecture": "amd64",
                "architectures": ["amd64"],
                "autopkgtest_backend": BackendType.AUTO,
                "binary_artifacts": [
                    {
                        "collection": "internal@collections",
                        "child_type": LookupChildType.ARTIFACT_OR_PROMISE,
                        "name_matcher": {
                            "kind": CollectionItemMatcherKind.STARTSWITH,
                            "value": "build-",
                        },
                    }
                ],
                "codename": "bookworm",
                "extra_repositories": None,
                "enable_autopkgtest": True,
                "enable_lintian": False,
                "enable_piuparts": False,
                "enable_reverse_dependencies_autopkgtest": False,
                "lintian_backend": BackendType.AUTO,
                "lintian_fail_on_severity": "none",
                "piuparts_backend": BackendType.AUTO,
                "reverse_dependencies_autopkgtest_suite": None,
                "source_artifact": self.source_artifact.id,
                "vendor": "debian",
            },
        )

    def test_populate_qa_piuparts(self) -> None:
        """Test populate create qa: piuparts enabled."""
        workflow = self.orchestrate(
            extra_data={
                "architectures": ["amd64"],
                "source_artifact": self.source_artifact.id,
                "enable_piuparts": True,
            }
        )

        self.assert_qa(
            workflow,
            {
                "arch_all_host_architecture": "amd64",
                "architectures": ["amd64"],
                "autopkgtest_backend": BackendType.AUTO,
                "binary_artifacts": [
                    {
                        "collection": "internal@collections",
                        "child_type": LookupChildType.ARTIFACT_OR_PROMISE,
                        "name_matcher": {
                            "kind": CollectionItemMatcherKind.STARTSWITH,
                            "value": "build-",
                        },
                    }
                ],
                "codename": "bookworm",
                "extra_repositories": None,
                "enable_autopkgtest": False,
                "enable_lintian": False,
                "enable_piuparts": True,
                "enable_reverse_dependencies_autopkgtest": False,
                "lintian_backend": BackendType.AUTO,
                "lintian_fail_on_severity": "none",
                "piuparts_backend": BackendType.AUTO,
                "reverse_dependencies_autopkgtest_suite": None,
                "source_artifact": self.source_artifact.id,
                "vendor": "debian",
            },
        )

    def test_populate_architectures_allow_deny(self) -> None:
        """Populate uses architectures, architectures_allow/deny."""
        workflow = self.orchestrate(
            extra_data={
                "source_artifact": self.source_artifact.id,
                "architectures": ["amd64", "i386", "arm64"],
                "architectures_allowlist": ["amd64", "i386"],
                "architectures_denylist": ["amd64"],
            }
        )

        sbuild = workflow.children.get(
            task_name="sbuild", task_type=TaskTypes.WORKFLOW
        )
        self.assertEqual(
            sbuild.task_data,
            {
                "arch_all_host_architecture": "amd64",
                "architectures": ["i386"],
                "backend": BackendType.AUTO,
                "environment_variant": None,
                "extra_repositories": None,
                "input": {"source_artifact": self.source_artifact.id},
                "target_distribution": "debian:bookworm",
                "signing_template_names": {},
            },
        )

    def test_populate_make_signed_source_missing_signed_source_purpose(
        self,
    ) -> None:
        """Populate raise WorkflowRunError: missing required field."""
        msg = (
            'orchestrator failed: "make_signed_source_purpose" must be '
            'set when signing the source'
        )
        with self.assertRaisesRegex(WorkflowRunError, msg):
            self.orchestrate(
                extra_data={
                    "source_artifact": self.source_artifact.id,
                    "architectures": ["amd64"],
                    "enable_make_signed_source": True,
                    "signing_template_names": {"amd64": ["hello"]},
                }
            )

    def test_populate_make_signed_source_missing_make_signed_source_key(
        self,
    ) -> None:
        """Populate raise WorkflowRunError: missing required field."""
        msg = (
            'orchestrator failed: "make_signed_source_key" must be set '
            'when signing the source'
        )
        with self.assertRaisesRegex(WorkflowRunError, msg):
            self.orchestrate(
                extra_data={
                    "source_artifact": self.source_artifact.id,
                    "architectures": ["amd64"],
                    "enable_make_signed_source": True,
                    "make_signed_source_purpose": KeyPurpose.OPENPGP,
                    "signing_template_names": {"amd64": ["hello"]},
                }
            )

    def test_populate_make_signed_source(self) -> None:
        """Populate create a make_signed_source workflow."""
        with patch.object(
            DebianPipelineWorkflow,
            "requires_artifact",
            wraps=DebianPipelineWorkflow.requires_artifact,
        ) as requires_artifact_wrapper:
            workflow = self.orchestrate(
                extra_data={
                    "architectures": ["amd64"],
                    "source_artifact": self.source_artifact.id,
                    "enable_make_signed_source": True,
                    "make_signed_source_purpose": KeyPurpose.OPENPGP,
                    "signing_template_names": {
                        "amd64": ["hello"],
                    },
                    "make_signed_source_key": 80,
                }
            )

        expected_binary_artifacts = [
            {
                "collection": "internal@collections",
                "child_type": LookupChildType.ARTIFACT_OR_PROMISE,
                "name_matcher": {
                    "kind": CollectionItemMatcherKind.STARTSWITH,
                    "value": "build-",
                },
            }
        ]
        make_signed_source = workflow.children.get(
            task_name="make_signed_source", task_type=TaskTypes.WORKFLOW
        )
        self.assertEqual(
            make_signed_source.task_data,
            {
                "architectures": ["amd64"],
                "binary_artifacts": expected_binary_artifacts,
                "codename": "bookworm",
                "key": 80,
                "purpose": "openpgp",
                "sbuild_backend": BackendType.AUTO,
                "signing_template_artifacts": [
                    "internal@collections/name:signing-template-amd64-hello"
                ],
                "vendor": "debian",
            },
        )

        self.assertEqual(
            make_signed_source.workflow_data_json,
            {
                "display_name": "make signed source",
                "step": "make_signed_source",
            },
        )

        # There are two calls to self.requires_artifact:
        # self.requires_artifact(wr, binary_artifacts)
        # self.requires_artifact(wr, LookupMultiple.parse_obj...)
        # But both of them are adding the same WorkRequest to depend on:
        # the sbuild work request (which generates the binary artifacts and the
        # signing artifacts
        self.assertQuerySetEqual(
            make_signed_source.dependencies.all(),
            list(
                WorkRequest.objects.filter(
                    task_type=TaskTypes.WORKER, task_name="sbuild"
                )
            ),
        )

        # Assert that the correct calls were made
        requires_artifact_wrapper.assert_has_calls(
            [
                call(
                    make_signed_source,
                    LookupMultiple.parse_obj(expected_binary_artifacts),
                ),
                call(
                    make_signed_source,
                    LookupMultiple.parse_obj(
                        [
                            "internal@collections/"
                            "name:signing-template-amd64-hello"
                        ]
                    ),
                ),
            ]
        )

        # MakeSignedSource.populate() was called and created its tasks
        self.assertTrue(make_signed_source.children.exists())
        for child in make_signed_source.children.filter():
            # The sbuild sub-workflow is running.  Everything else is
            # blocked.
            self.assertEqual(
                child.status,
                (
                    WorkRequest.Statuses.RUNNING
                    if child.task_type == TaskTypes.WORKFLOW
                    else WorkRequest.Statuses.BLOCKED
                ),
            )

    def test_populate_package_upload(self) -> None:
        """Populate create an upload_package workflow."""
        workflow = self.orchestrate(
            extra_data={
                "source_artifact": self.source_artifact.id,
                "architectures": ["amd64", "i386"],
                "enable_make_signed_source": True,
                "make_signed_source_purpose": KeyPurpose.OPENPGP,
                "signing_template_names": {"amd64": ["hello"]},
                "make_signed_source_key": 80,
                "enable_upload": True,
                "vendor": "debian",
                "codename": "trixie",
                "upload_merge_uploads": False,
                "upload_since_version": "1.1",
                "upload_target": "ftp://user@example.org/pub/UploadQueue1/",
                "upload_target_distribution": "debian:testing",
            }
        )

        package_upload = workflow.children.get(
            task_name="package_upload", task_type=TaskTypes.WORKFLOW
        )

        self.assertEqual(
            package_upload.task_data,
            {
                "binary_artifacts": [
                    {
                        "collection": "internal@collections",
                        "child_type": LookupChildType.ARTIFACT_OR_PROMISE,
                        "name_matcher": {
                            "kind": CollectionItemMatcherKind.STARTSWITH,
                            "value": "build-",
                        },
                    }
                ],
                "codename": "trixie",
                "delayed_days": None,
                "key": None,
                "merge_uploads": False,
                "require_signature": True,
                "since_version": "1.1",
                "source_artifact": self.source_artifact.id,
                "target": "ftp://user@example.org/pub/UploadQueue1/",
                "target_distribution": "debian:testing",
                "vendor": "debian",
            },
        )

        self.assertEqual(
            package_upload.workflow_data_json,
            {"display_name": "package upload", "step": "package_upload"},
        )

        self.assertQuerySetEqual(
            package_upload.dependencies.all(),
            list(
                WorkRequest.objects.filter(
                    task_type=TaskTypes.WORKER, task_name="sbuild"
                )
            ),
        )

        # PackageUpload.populate() was called and created its tasks
        self.assertTrue(package_upload.children.exists())
        for child in package_upload.children.all():
            # MakeSourcePackageUpload only requires the source artifact and
            # so can start immediately.  Everything else is blocked.
            self.assertEqual(
                child.status,
                (
                    WorkRequest.Statuses.PENDING
                    if child.task_name == "makesourcepackageupload"
                    else WorkRequest.Statuses.BLOCKED
                ),
            )

    def test_populate_upload_package_no_include_source_binaries(self) -> None:
        """Populate create upload_workflow without source neither binaries."""
        workflow = self.orchestrate(
            extra_data={
                "architectures": ["amd64"],
                "source_artifact": self.source_artifact.id,
                "enable_make_signed_source": True,
                "make_signed_source_purpose": KeyPurpose.OPENPGP,
                "signing_template_names": {"amd64": ["hello"]},
                "make_signed_source_key": 80,
                "enable_upload": True,
                "vendor": "debian",
                "codename": "trixie",
                "upload_merge_uploads": False,
                "upload_since_version": "1.1",
                "upload_target": "ftp://user@example.org/pub/UploadQueue1/",
                "upload_include_source": False,
                "upload_include_binaries": False,
            }
        )

        package_upload = workflow.children.get(
            task_name="package_upload", task_type=TaskTypes.WORKFLOW
        )

        self.assertEqual(
            package_upload.task_data,
            {
                "binary_artifacts": [],
                "codename": "trixie",
                "delayed_days": None,
                "key": None,
                "merge_uploads": False,
                "require_signature": True,
                "since_version": "1.1",
                "source_artifact": None,
                "target": "ftp://user@example.org/pub/UploadQueue1/",
                "target_distribution": None,
                "vendor": "debian",
            },
        )

        self.assertEqual(
            package_upload.workflow_data_json,
            {"display_name": "package upload", "step": "package_upload"},
        )

    def test_populate_package_upload_delayed(self) -> None:
        """Populate an upload_package workflow for a delayed upload."""
        workflow = self.orchestrate(
            extra_data={
                "source_artifact": self.source_artifact.id,
                "architectures": ["amd64", "i386"],
                "enable_upload": True,
                "vendor": "debian",
                "codename": "trixie",
                "upload_merge_uploads": False,
                "upload_since_version": "1.1",
                "upload_target": "ftp://user@example.org/pub/UploadQueue1/",
                "upload_target_distribution": "debian:testing",
                "upload_delayed_days": 3,
            }
        )

        package_upload = workflow.children.get(
            task_name="package_upload", task_type=TaskTypes.WORKFLOW
        )

        self.assertEqual(
            package_upload.task_data,
            {
                "binary_artifacts": [
                    {
                        "collection": "internal@collections",
                        "child_type": LookupChildType.ARTIFACT_OR_PROMISE,
                        "name_matcher": {
                            "kind": CollectionItemMatcherKind.STARTSWITH,
                            "value": "build-",
                        },
                    }
                ],
                "codename": "trixie",
                "delayed_days": 3,
                "key": None,
                "merge_uploads": False,
                "require_signature": True,
                "since_version": "1.1",
                "source_artifact": self.source_artifact.id,
                "target": "ftp://user@example.org/pub/UploadQueue1/",
                "target_distribution": "debian:testing",
                "vendor": "debian",
            },
        )

        self.assertEqual(
            package_upload.workflow_data_json,
            {"display_name": "package upload", "step": "package_upload"},
        )

        self.assertQuerySetEqual(
            package_upload.dependencies.all(),
            list(
                WorkRequest.objects.filter(
                    task_type=TaskTypes.WORKER, task_name="sbuild"
                )
            ),
        )

        # PackageUpload.populate() was called and created its tasks
        self.assertTrue(package_upload.children.exists())
        for child in package_upload.children.all():
            # MakeSourcePackageUpload only requires the source artifact and
            # so can start immediately.  Everything else is blocked.
            self.assertEqual(
                child.status,
                (
                    WorkRequest.Statuses.PENDING
                    if child.task_name == "makesourcepackageupload"
                    else WorkRequest.Statuses.BLOCKED
                ),
            )

    def test_get_label(self) -> None:
        """Test get_label."""
        w = self.create_debian_pipeline_workflow(extra_task_data={})
        self.assertEqual(w.get_label(), "run Debian pipeline")
