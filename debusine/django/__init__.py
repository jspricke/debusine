# Copyright 2024 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""
Common Django utility code.

Modules in this package require Django, though the python3-debusine package
doesn't depend on it.  As a result, they should only be used from other
parts of debusine that require Django and declare a proper dependency.
"""
