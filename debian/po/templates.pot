# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the debusine package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: debusine\n"
"Report-Msgid-Bugs-To: debusine@packages.debian.org\n"
"POT-Creation-Date: 2024-07-15 16:24+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: select
#. Description
#: ../debusine-server.templates:1001
msgid "Web server to configure automatically:"
msgstr ""

#. Type: select
#. Description
#: ../debusine-server.templates:1001
msgid ""
"debusine-server supports any web server that supports proxying to a Unix-"
"domain socket, but only nginx can be configured automatically."
msgstr ""

#. Type: select
#. Description
#: ../debusine-server.templates:1001
msgid ""
"Please select the web server that should be configured automatically for "
"debusine-server."
msgstr ""

#. Type: note
#. Description
#: ../debusine-server.templates:2001
msgid "Configuring debusine-server in Nginx"
msgstr ""

#. Type: note
#. Description
#: ../debusine-server.templates:2001
msgid ""
"The debusine-server Nginx configuration file is a vhost configuration. "
"Hence, it comes with a server name which is set to localhost. You will have "
"to change it properly."
msgstr ""

#. Type: boolean
#. Description
#: ../debusine-server.templates:3001
msgid "Should the web server be restarted now?"
msgstr ""

#. Type: boolean
#. Description
#: ../debusine-server.templates:3001
msgid ""
"In order to activate the new configuration, the configured web server has to "
"be restarted."
msgstr ""
