History
-------

Debusine has been initiated by Raphaël Hertzog in 2019 but it was mostly
dormant until Freexian started to fund its development in 2021 by paying
for the work of Carles Pina i Estany. In 2023, Freexian obtained funding
from SovereignTechFund.de to accelerate the pace of its development and
achieve some significant milestones.

Copyright notice
----------------

© 2019-2024 Freexian SARL
© 2019-2024 The Debusine developers

Current team
------------

* Carles Pina i Estany
* Colin Watson
* Enrico Zini
* Jochen Sprickerhof
* Raphaël Hertzog (maintainer)
* Stefano Rivera
* Sylvain Beucler

Other contributors and former team members (by alphabetical order)
------------------------------------------------------------------

* Neil Williams
* Emilio Pozuelo Monfort

Freexian relationships
----------------------

This section lists the periods during which contributors worked on
debusine on behalf of Freexian:

* Carles Pina i Estany: from 2021-06 to - (still active)
* Colin Watson: from 2024-01 to - (still active)
* Emilio Pozuelo Monfort: from 2023-09 to 2024-06
* Enrico Zini: from 2023-02 to - (still active)
* Jochen Sprickerhof: from 2023-11 to - (still active)
* Neil Williams: from 2021-07 to 2021-11
* Raphaël Hertzog: from 2019-07 to - (still active)
* Stefano Rivera: from 2023-09 to - (still active)
* Sylvain Beucler: from 2023-09 to - (still active)
