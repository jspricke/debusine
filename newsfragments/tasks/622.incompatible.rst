:ref:`Autopkgtest <task-autopkgtest>`: Replace the ``extra_apt_sources`` property with ``extra_repositories``, following the same syntax as :ref:`Sbuild <task-sbuild>`.
