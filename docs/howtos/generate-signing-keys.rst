=====================
Generate signing keys
=====================

After :ref:`setting up a signing worker <set-up-debusine-signing>`, you may
want to generate signing keys for use by tasks.  (See also
:ref:`configure-hsm` for high-value keys.)

This process currently requires administrative access to the server.  In
future it may be made available via a workflow or run automatically for new
workspaces.

.. code-block:: console

   $ sudo -u debusine-server \
       debusine-admin create_work_request signing generatekey --data - <<END
   purpose: uefi
   description: Some description of the new key
   END

This generates a new key and stores it as an artifact, which will have no
expiry of its own (although it will expire if the workspace has a non-zero
default expiration delay).  You can find the fingerprint of the new key by
looking up the work request in the web interface.
