.. _debusine-package-repositories:

====================
Package repositories
====================

You can install debusine from two different package repositories.

The Debian repository
---------------------

debusine is available in the Debian archive, in sid (unstable) and trixie
(testing).

If you want to run debusine on Debian 12, you will need to enable the
bookworm-backports repository and install python3-django from that
repository:

.. code-block:: console

  # Only needed on Bookworm
  $ sudo tee /etc/apt/sources.list.d/bookworm-backports.list <<END
  deb http://deb.debian.org/debian bookworm-backports main
  END
  $ sudo apt update
  $ sudo apt install python3-django/bookworm-backports

The snapshot repository
-----------------------

This repository contains packages built out of the ``devel`` branch of
debusine's git repository. It can be used to test the next release that is
still in development.

.. include:: /common/add-snapshot-repository.rst
