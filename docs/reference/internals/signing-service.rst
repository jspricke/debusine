.. _reference-signing-service:

Signing service details
=======================

Backend overview
----------------

There is a ``debusine.signing`` application, which is a separate Django
application under the ``debusine`` project with its database models routed
to a separate database, such that it can run on a separate system from the
debusine server and workers.  It uses Django primarily for its database
facilities: there is initially no need for it to have its own server
component, although it may eventually be useful to add one for things like
reading audit logs.

This application has a worker, reusing most of the existing
``debusine.worker`` code.  It sends metadata to the server indicating that
it is a :ref:`Signing worker <explanation-workers>`, and that it requires
HTTPS connectivity to the server.  ``debusine-admin manage_worker`` only
manages signing workers if given the ``--worker-type signing`` option, to
avoid enabling them by accident.

:ref:`Signing tasks <task-type-signing>` are scheduled similarly to worker
tasks, and are required to use the public API to interact with artifacts in
the same way that worker tasks do, but they only execute on signing workers.
Signing workers do not take tasks of any other type.

.. todo::

   Specify how to configure keys to be used with a YubiKey.

Each successful ``generate`` and ``sign`` operation adds a row to an
append-only audit log table.

Database models
---------------

Each key has a row with the following fields:

* ``purpose``: the purpose of this key (e.g. ``uefi`` for UEFI Secure Boot)
  different key purposes typically require different tools to generate them
  or sign data using them
* ``fingerprint``: the key fingerprint; keys are unique by purpose and
  fingerprint
* ``private_key``: a :ref:`protected representation <key-protection>` of the
  private key
* ``public_key``: the public key, as binary data
* ``created_at``, ``updated_at``: timestamps for creation and update

HSM key availability
--------------------

If keys are stored in a hardware security module such as a YubiKey, then
they may not be available to all signing workers.  A worker can add the list
of such keys it supports to its dynamic metadata, and then the
``can_run_on`` method of the relevant tasks can check that metadata to avoid
dispatching requests to workers that do not have access to the relevant
keys.

.. todo::

   Add more precise details of how this is recorded.
