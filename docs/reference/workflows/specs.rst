.. _available-workflows:

===================
Available workflows
===================

Most workflows are mainly useful as sub-workflows of a more complex
workflow. The workflows that you are most likely to use directly
is the :ref:`debian_pipeline workflow <workflow-debian-pipeline>`.

You will launch a :ref:`workflow template <workflow-template>`, which
provides some default settings, rather than directly launching the bare
workflow.
These are configured by debusine instance admins, and are listed on the
Workspace's details page. (e.g. for `debusine.debian.net
<https://debusine.debian.net/debusine/System/view/>`_).

.. toctree::
   :glob:

   specs/*
