.. _workflow-group:

Group of work requests
======================

When a workflow generates a large number of related/similar work requests,
it might want to hide all those work requests behind a group that would
appear a single step in the visual representation of the workflow.  This is
implemented by a ``group`` key in the ``workflow_data`` dictionary of each
task.
