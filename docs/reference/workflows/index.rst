.. _workflow-reference:

=========
Workflows
=========

Workflows are server-side logic that can schedule and combine server
tasks and worker tasks to automate complex operations.

Workflows are created from a workflow template chosen from a set maintained by
the server administrators, plus data coming from user input.

See the :ref:`explanation-workflows` for an overview, and below for
technical details.


.. toctree::

    implementation
    event-reactions
    group
    template
    specs
    advanced-sub-workflows