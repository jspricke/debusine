.. _workflow-sbuild:

Workflow ``sbuild``
===================

This workflow takes a source package and creates sbuild work requests (see
:ref:`task-sbuild`) to build it for a set of architectures.

* ``task_data``:

  * ``prefix`` (optional): prefix this string to the item names provided in
    the internal collection

  * ``input`` (required): see :ref:`package-build-task`
  * ``target_distribution`` (required string): ``vendor:codename`` to specify
    the environment to use for building. It will be used to determine
    ``distribution`` or ``environment``, depending on ``backend``.
  * ``backend`` (optional string): see :ref:`package-build-task`
  * ``architectures`` (required list of strings): list of architectures to
    build. It can include ``all`` to build a binary for ``Architecture: all``
  * ``arch_all_host_architecture`` (string, defaults to ``amd64``): concrete
    architecture on which to build ``Architecture: all`` packages
  * ``extra_repositories`` (optional, default unset): enable extra APT
    repositories, see :ref:`package-build-task`.
  * ``environment_variant`` (optional string): variant of the
    environment we want to build on, e.g. ``buildd``; appended during
    environment :ref:`lookup <lookup-syntax>` for
    ``target_distribution`` above.
  * ``build_profiles`` (optional, default unset): select a build profile, see
    :ref:`package-build-task`.
  * ``binnmu`` (optional, default unset): build a binNMU, see
    :ref:`package-build-task`.
  * ``retry_delays`` (optional list): a list of delays to apply to each
    successive retry; each item is an integer suffixed with ``m`` for
    minutes, ``h`` for hours, ``d`` for days, or ``w`` for weeks.
  * ``signing_template_names`` (dictionary, optional): mapping from
    architecture to list of names of binary packages that should be used as
    templates by the :ref:`task-extract-for-signing`

The source package will be built on the intersection of the provided list of
architectures and the architectures supported in the ``Architecture:`` field
of the source package. Architecture ``all`` packages are built on
``arch_all_host_architecture``.

The workflow may also apply a denylist of architectures if it finds a
``debian:suite`` collection corresponding to the build
distribution/environment, and that suite provides one.

The workflow adds event reactions that cause the ``debian:upload`` artifact
in the output for each architecture to be provided as
``{prefix}build-{architecture}`` in the workflow's internal collection.

If the workspace has a :ref:`debian:package-build-logs collection
<collection-package-build-logs>`, then the workflow adds
:ref:`action-update-collection-with-data` and
:ref:`action-update-collection-with-artifacts` event reactions to each
sbuild work request to record their build logs there.

If ``retry_delays`` is set, then the workflow adds a corresponding
``on_failure`` :ref:`action-retry-with-delays` action to each of the sbuild
work requests it creates.  This provides a simplistic way to retry
dependency-wait failures.  Note that this currently retries any failure, not
just dependency-waits; this may change in future.

If ``signing_template_names`` exists, then the workflow adds event reactions
that cause the corresponding ``debian:binary-package`` artifacts in the
output for each architecture to be provided as
``{prefix}signing-template-{architecture}-{binary_package_name}`` in the
workflow's internal collection.
