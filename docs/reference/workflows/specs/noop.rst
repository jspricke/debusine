.. _workflow-noop:

Workflow ``noop``
=================

This is a workflow that does nothing, and is mainly used in tests.

* ``task_data``: empty


