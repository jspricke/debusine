.. _task-reference:

=====
Tasks
=====

.. toctree::

    task-types
    ontology-generic-tasks
    specs
