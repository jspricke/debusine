.. _available-tasks:

This section lists all the available tasks, with the input that they
are accepting, the description of what they are doing, including
the artifacts that they are generating.

The tasks are categorized by their :ref:`task type
<reference-task-types>`. They can all be used as part of :ref:`workflows
<workflow-reference>` but worker tasks can also be scheduled individually.

.. _available-worker-tasks:

Available worker tasks
======================

.. toctree::
   :glob:

   worker/*

.. _available-server-tasks:

Available server tasks
======================

.. toctree::
   :glob:

   server/*

.. _available-signing-tasks:

Available signing tasks
=======================

.. toctree::
   :glob:

   signing/*

.. _available-wait-tasks:

Available wait tasks
====================

.. toctree::
   :glob:

   wait/*
