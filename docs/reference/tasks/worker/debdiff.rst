.. _task-debdiff:

DebDiff task
------------

A task to compute the differences between two source or binary Debian packages using the ``debdiff`` utility.

The ``task_data`` associated to this task can contain the following keys:

* ``input`` (required): a dictionary describing the input data

  * ``source_artifacts`` (optional): a list with two elements.
    Each of type (:ref:`lookup-single`): source artifact pointing to a source package.
  * ``binary_artifacts:`` (optional): a list with two elements.
    Each of type (:ref:`lookup-multiple`): binary artifact pointing to binary packages.

.. note::

  Note that exactly one of `source_artifacts` or `binary_artifacts` is required.

* ``extra_flags`` (optional): a list of flags to be passed to the debdiff command, such as ``--nocontrol`` or ``--diffstat``.
  If an unsupported flag is passed then the request will fail.

The ``debdiff`` output will be provided as a new artifact of category ``debian:debdiff``, described in the :ref:`artifacts reference <artifact-debdiff>`.

The task returns success if ``debdiff`` returns an exit code of 0 or 1, and failure otherwise.
