.. _task-generate-key:

GenerateKey task
----------------

This is a :ref:`signing task <task-type-signing>` that generates a new key
on a signing worker and stores it for later use.

The ``task_data`` for this task may contain the following keys:

* ``purpose`` (required): the purpose of the key to generate: ``uefi``,
  or ``openpgp``.
* ``description`` (required): A text string with a human-readable
  description of the new key's intended purpose.

The output will be provided as a :ref:`debusine:signing-key
<artifact-signing-key>` artifact.  The task should typically be configured
with an :ref:`action-update-collection-with-artifacts` event reaction to
attach the new signing key to the collection where it will be used, such as
:ref:`debian:suite-signing-keys <collection-suite-signing-keys>`.

.. todo::

   This will need additional parameters once we start supporting HSMs.
