===================================
User interface to monitor workflows
===================================

User story / requirements
=========================

As a debusine user, I can easily switch between different workspaces and
have a list of running and/or historical workflows for each of the
workspaces.

I can filter the list of workflows following different "fields" and sort
it against one of the fields. I can bookmark the resulting view and access
it directly in the future.

Among the fields shown there are:

- the ID of the workflow
- the name of the workflow template used to start the workflow combined
  with some user-friendly representation of its main input parameters
  (e.g. ``UploadToUnstable(dpkg_1.4.23)`` or
  ``SubmitEmbargoedUpdate(glibc_2.40-3+deb12u1)``)
- the status + result of the workflow (likely shown in the same cell to
  save space, status shown except when it's completed, in which case
  we show the result?)
- the runtime status of the workflow, a value computed based on the status
  of the child work requests (see below)
- the ``started_at`` timestamp of the workflow
- the ``completed_at`` timestamp of the workflow
- the total number of (non-superseded) child work requests split between
  successful and failed work requests and still pending or blocked work
  requests
- the last activity (the most recent ``completed_at`` or
  ``started_at`` among the child work requests)
- the user who started the workflow

It must be possible to filter the list in the following ways:

- restrict to one or more specific workflow templates
- restrict to one or more status (defaults to show only the RUNNING
  workflows)
- restrict to one or more runtime status
- restrict to one or more users who initiated the workflows
- restrict to workflows with failed work requests

The list should be sortable according to all the figures and timestamps.
The filters and sorting can be setup/updated with a form embedded in the
page.

Clicking on the name of the workflow brings you to the detailed work
request view showing the progress of the workflow.

Clicking on the total number of work requests inside the workflow brings
you to the list of work-requests with a filter restricting work requests
to said workflow. Clicking on the number of
failed/successful/not-completed work requests brings you to the same list
with an extra filter to restrict to the corresponding subset.

If the runtime status is "Needs input", then this label links to the
(first) work-request that actually needs input.

Runtime status of the workflow 
------------------------------

The runtime status can have the following values (order of checks matters
here, first match defines the status):

- "Needs input": there's at least one child work request with task type
  WAIT and status == RUNNING where ``workflow_data['needs_input']`` is true.
  
- "Running": there's at least one child work request with task type !=
  WAIT and status == RUNNING.

- "Waiting": there's at least one child work request with task type
  WAIT and status == RUNNING where ``workflow_data['needs_input']`` is false.
 
- "Pending": there's at least one child work request where status ==
  PENDING.

- "Blocked": there's at least one child work request where status == BLOCKED.

- "Aborted": there's at least one child work request where status == ABORTED.

- "Completed": all the child work requests are in status COMPLETED.

Expected changes
================

* Add new filters to the list of work-requests:

  * workflow: restrict to work-requests part of the given workflow
  * status: restrict to work requests having one of the listed "status"
  * result: restrict to work requests having one of the listed "result"
  * task_type: restrict to work requests having one of the listed "task_type"

* Create new view on ``/<scope>/<workspace>/workflows/`` implementing the
  list of workflows described above.

* Overhaul the user interface to be workspace centered.

  * the list of work requests is restricted to the current workspace and
    is available at ``/<scope>/<workspace>/work-request/``

  * the menu bar shows the current workspace on the top left, just after
    the "Debusine" link bringing you back to the home page. It should have
    a drop-down menu to let you switch to another workspace that you have
    access to.

  * inside a workspace, the menu bar should have a "Workflows" menu entry.
    Clicking the entry brings you to the list of workflows of that
    workspace. Opening the menu shows you the list of WorkflowTemplate
    and clicking on a specific workflow template brings you to the list of
    workflows filtered to only show this specific template.

  * inside a workspace, the menu bar should have a "Collections" menu
    entry. Clicking that entry brings you to the list of collections.
    Opening the menu shows you the list of collections and clicking on
    that collection brings you to the main view of the collection.

  * the "Workers" and "Tasks status" pages should be moved outside of scopes
    in top-level pages (maybe ``/-/workers`` and ``/-/queues`` ?) and the
    links should be dropped from the top-level menu. They can be hidden in
    some sub-menu or in the footer.

  * the ``/<scope>/`` page should list the workspaces available in that
    scope that are visible to the current user (provided that the scope
    itself is visible/accessible to the user)

  * the ``/<scope>/<workspace>/`` page is extended to show figures about
    workflows. It has a list of workflow templates along with count of:

    * running workflows
    * workflows that need user input
    * completed workflows

    Each of these figures link back to the list of workflows with the
    corresponding filters applied.

  * the "Create artifact" and "Create work request" plumbing pages should
    become workspace specific (and thus there's no need to select
    the workspace explicitly as part of the form).
