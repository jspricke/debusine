.. _development-blueprints:

======================
Development blueprints
======================

.. toctree::

    permissions
    permission-predicates
    signing-permissions
    ui-views
    package-upload
    task-configuration
    task-statistics
    build-instructions
    scoped-urls
    debian-pipeline
    monitor-workflows
    url-redesign
